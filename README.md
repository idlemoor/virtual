# Virtual Slackware package generator

Experimental system to automatically create Slackware packages from
various software ecosystems.

Initial support will include cpan, npm, rubygems and pypi.

Why is this better than using cpan, npm, gem and pip directly?

  * Installed system-wide, not for one user
  * Software from different ecosystems can interwork
  * Consistency between ecosystems, only one tool to learn
  * Clean installs, clean updates, clean removals
  * Reproducibility, reliability, transportability
  * Adherence to Slackware/SlackBuilds conventions (principle of least surprise)
